<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S4-A1</title>
</head>
<body>

<!-- activity4 -->
	<h1>Building</h1>
	<?php echo $building->getName(); ?> </br>
	<?php echo $building->getFloors(); ?> </br>
	<?php echo $building->getAddress(); ?> </br>
	<?php $building->setChangedName('Caswynn Complex'); ?>
	<?php echo $building->getChangedName(); ?> 
	


	<h1>Condominium</h1>
	<?php $condominium->setName('Enzo Tower'); ?>
	<?php echo $condominium->getName(); ?> </br>
	<!-- <?php $condominium->$floors; ?> -->
	<?php echo $condominium->getFloors(); ?> </br>
	<?php echo $condominium->getAddress(); ?> </br>
	<?php $condominium->setChangedName('Enzo Condo'); ?>
	<?php echo $condominium->getChangedName(); ?> 
</body>
</html>
