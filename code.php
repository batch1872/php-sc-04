<?php

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
	//get and set  building's name
	public function getName(){
		return "The name of the building is $this->name.";
	}

	public function setName($name){
		$this->name = $name;
	}
	//get and set  building's floors
	public function getFloors(){
		return "The $this->name has $this->floors floors.";
	
	}

	public function setFloors($floors){
		$this->floors = $floors;
	}
	//get and set building's address
	public function getAddress(){
		return "The $this->name is located at $this->address.";
	
	}

	public function setAddress($floors){
		$this->address = $address;
	}
	//get and set building's changedName..
	public function getChangedName(){
		return "The name of the building has been changed to $this->name.";
	
	}

	public function setChangedName($name){
		$this->name = $name;
		
	}
}


class Condominium extends Building {
	



}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
